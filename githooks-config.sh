#!/bin/bash 

function windows_clean {
 echo "Cleaning"
 rm -f .git/hooks/*
}

function windows_install {
  cmd //c "mklink .git\\hooks\\prepare-commit-msg ..\\..\\prepare-commit-msg.rb"
}

function help {
  echo "Help"
  echo "Default is 'install'"
  echo "install -->  add the links to the git hooks dir"
  echo "clean -->  remove the links to from git hooks dir"
}

SH_CMD=$1

if [ -z $SH_CMD ]; then
  SH_CMD="install"
fi

PLATFORM="null"

chmod +x *.rb

if [ "$(uname)" == "Darwin" ]; then
  PLATFORM="mac";        
elif [[ "$(uname)" =~ ^Linux.* ]]; then
  PLATFORM="linux"
elif [[ "$(uname)" =~ ^MINGW.* ]]; then
  PLATFORM="pc"
else
  echo "Current Platform not supported"
  return 0
fi

if [ "$PLATFORM" == "pc" ]; then
  if [ "install" = $SH_CMD ]; then
    windows_install
  elif [ "clean" = $SH_CMD ]; then
    windows_clean
  else
	help
  fi
fi
return 0



