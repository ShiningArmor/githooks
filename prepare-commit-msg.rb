#!/usr/bin/env ruby

require 'open3'
require 'logger'

require 'logger'

logger = Logger.new(STDOUT)

#
# Returns stdout on success, Runtime Error on failure or error
#
def syscall(*cmd)
  begin
    stdout, stderr, status = Open3.capture3(*cmd)
	
    if status.success?
      return stdout.slice!(0..-(1 + $/.size)) # strip trailing eol
	else
      raise "Error exec command: #{cmd};stdout=#{stdout};stderr=#{stderr};status=#{status}"
    end
  end
end

logger.debug( "------- Start" )
logger.debug "------- PrePare Commit Message"

message_file = ARGV[0]
message_source = ARGV[1]
message_option = ARGV[3]

logger.debug( "In File: #{message_file}")
logger.debug( "In Source: #{message_source}")
logger.debug( "In Option: #{message_option}")

current_branch = syscall 'git rev-parse --abbrev-ref HEAD'

#if "master".casecmp( current_branch )
#  raise "Error, can not commit to branch 'master'"
#end

#Note: Jira requires the key to be uppercase
jira_id = current_branch.upcase.match("^[A-Z]*-[0-9]*")

logger.debug(  "Branch:  #{current_branch}" )

#Not sure if I want to enforce this here.... yet
#if ( jira_id.nil? || jira_id.empty? )
#  raise "Could not find JIRA issue ID in branch name.  Branch must start with JIRA-ID"
#end
# Make the JIRA-ID from the Branch optional for now
if ( jira_id.nil? || jira_id.empty? )
  exit 0
end

logger.debug(  "JIRA-ID: #{jira_id} " )

#Automagically put the JIRA-ID at the start of the file

# For now assume the commit descriptions are 'small'
message_contents = []
File.open(message_file) do |f|
  f.each_line {|line|
    message_contents.push line
  }
end

#Overwrite the file, adding the JIRA-ID
File.open(message_file, 'w') do |fo|
  fo.puts jira_id
  message_contents.each {|line|
    fo.puts line
  }
end


logger.debug( "------- PrePare Commit Message")

